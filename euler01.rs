
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// euler01.rs
// Copyright (C) 2018 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////


fn main() {
    let mut sum = 0;
    for num in 1..1000 {
        if num % 3 == 0 || num % 5 == 0 {
        sum += num 
        }
    }

    println!("{}", sum);
    

}

