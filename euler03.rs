/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// primefactors.rs
// Copyright (C) 2018 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
// Euler 03
// find the greatest prime factor of a number
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

use std::time::Instant; // calculate elapsed time

fn main() {
    let start = Instant::now();

    let number = 600851475143; // number to factor
    let limit = (number as f64).sqrt() as i64 + 1; // upper limit of values to check

    for factor in (3..limit).step_by(2) {
        // step by 2 since primes are odd
        if number % factor == 0 && is_prime(factor) {
            // check if it is a factor and prime
            println!("prime factor {}", factor);
        }
    }
    let duration = start.elapsed();

    println!("elasped time: {:?}", duration)
}

fn is_prime(num: i64) -> bool {
    // check if a number is prime and return true/false

    if num == 2 || num == 3 {
        return true;
    }

    if num % 2 == 0 || num % 3 == 0 {
        return false;
    }

    let upper = (num as f64).sqrt() as i64 + 1;

    for div in 4..upper {
        if num % div == 0 {
            return false;
        }
    }
    true
}
