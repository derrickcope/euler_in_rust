/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// palin.rs
// Copyright (C) 2018 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

use std::time::Instant;
fn main() {

    let start = Instant::now();

    let mut greatest = 0;
    for x in 100 .. 1000 {
        for y in x .. 1000 {// no need to start from 100 again
            let sum = x * y;
           if is_palin(sum) && sum > greatest {
                    greatest = sum;
            }   
        }
    }

    let duration = start.elapsed();
    println!("{}", greatest);
    println!("elasped time: {:?}", duration)

}

fn is_palin(can: u64) -> bool {
    let can = can.to_string();
    let v: Vec<&str> = can.split("").collect();
    let rv: Vec<&str> = can.rsplit("").collect();

    if v == rv {
        return true
    }
    false
}
