/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// euler10.rs
// Copyright (C) 2018 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

use std::time::Instant;
fn main() {
    let start = Instant::now();
    let mut sum = 2;
    let upper = 2_000_000;

    for can in (3..upper + 1).step_by(2) {
        if is_prime(can) {
            sum += can;
        }
    }
    println!("{}", sum);

    let duration = start.elapsed();

    println!("time: {:?}", duration);
}

fn is_prime(num: i64) -> bool {
    // check if a number is prime and return true/false

    if num == 2 || num == 3 {
        return true;
    }

    if num % 2 == 0 || num % 3 == 0 {
        return false;
    }

    let upper = (num as f64).sqrt() as i64 + 1;

    for div in 4..upper {
        if num % div == 0 {
            return false;
        }
    }
    true
}
