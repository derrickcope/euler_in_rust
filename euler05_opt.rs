
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// all.rs
// Copyright (C) 2018 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

use std::process::exit;
use std::time::Instant;

fn main() {

    let start = Instant::now();

    for dsor in (2520 .. 300_000_000).step_by(2520) {
    
        if (3 .. 21).all(|x| dsor % x == 0) {
            let answer = dsor;
            println!("{}", answer);
            let duration = start.elapsed();
            println!("time: {:?}", duration);
            exit(0);
        }
    }

}
