
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
// euler07.rs
// Copyright (C) 2018 derrick cope <derrick(at)thecopes.me>
// Distributed under terms of the MIT license.
//
//////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////

use std::process::exit;
use std::time::Instant;

fn main() {

    let start = Instant::now();
    let mut count = 1;

    for can in (3 .. 1_000_000).step_by(2) { // primes are odd except for 2
        
        if prime::is_prime(can) {
            count += 1;
        }

        if count == 10_001 {
            let duration = start.elapsed();
            println!("{} is the {} prime", can, count);
            println!("elapsed time: {:?}", duration);
            exit(0)
        }
    }
}


mod prime {
pub fn is_prime(num: i64) -> bool {
    // check if a number is prime and return true/false

    if num == 2 || num == 3 {
        return true;

    } else if num % 2 == 0 || num % 3 == 0 {
        return false;
    }

    let upper = (num as f64).sqrt() as i64 + 1;

    for div in 4..upper {
        if num % div == 0 {
            return false;
        }
    }
    true
}
}
